#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include "matrix.h"
#include "vector.h"


int main(int argc, char *argv[]) {
printf("ENTER 1. FOR MATRIX MULTIPLICATION AND 2. FOR VECTOR MULTIPLICATION: \n");
int a=0;
scanf("%d", &a);
   pthread_t threads[NUM_THREADS];
   int *taskids[NUM_THREADS];
   int rc, i, t, rows, columns;
int j=0;

int marray1[10*10];
int marray2[10*10];
int result1[10*10];
if(a==1){
marray2[10*10];
result1[10*10];}

if(a==2){
marray2[10*1];
result1[10*1];}

   rows = 10;
   columns = 10;

if(a==1){
   for(i=0; i<(10*10); i++){
	marray1[i] = rand() % 11;
	marray2[i] = rand() % 11;
   }}

if(a==2){
for(i=0; i<(10*10); i++){
	marray1[i] = rand() % 11;
   }

   for(i=0; i<(10*1); i++){
	marray2[i] = rand() % 11;
   }}

printf("First matrix: \n");
   for (i = 0; i < (10*10); i += 10) {
	for (j = 0; j < 10; j++) {
		printf("%d\t", marray1[i + j]);
	}
	printf("\n");
   }

printf("\n");
if(a==1){
printf("Second matrix: \n");
for (i = 0; i < (10*10); i += 10) {
	for (j = 0; j < 10; j++) {
		printf("%d\t", marray2[i + j]);
	}
	printf("\n");
   }}
if(a==2){
printf("Second matrix: \n");
for (i = 0; i < (10*1); i += 1) {
		printf("%d\t", marray2[i]);
	printf("\n");
   }}
printf("\n");

if(a==1){
for (i = 0; i < (10*10); i++) {
			result1[i] = 0;
}}

if(a==2){
for (i = 0; i < (10*1); i++) {
			result1[i] = 0;
}}

if(a==1){
for(t=0;t<NUM_THREADS;t++) {
  matrix_data_array[t].thread_id = t;
  matrix_data_array[t].rows = rows;
  matrix_data_array[t].columns = columns;
  matrix_data_array[t].marray1 = marray1;
  matrix_data_array[t].marray2 = marray2;
  matrix_data_array[t].result1 = result1;
  printf("Creating thread %d\n", t);

  rc = pthread_create(&threads[t], NULL, MatrixMult, (void *) 
       &matrix_data_array[t]);
  if (rc) {
    printf("ERROR; return code from pthread_create() is %d\n", rc);
    exit(-1);
    }
  }}

if(a==2){

for(t=0;t<NUM_THREADS;t++) {
  matrix_data_array1[t].thread_id = t;
  matrix_data_array1[t].rows = rows;
  matrix_data_array1[t].columns = columns;
  matrix_data_array1[t].marray1 = marray1;
  matrix_data_array1[t].marray2 = marray2;
  matrix_data_array1[t].result1 = result1;
  printf("Creating thread %d\n", t);

  rc = pthread_create(&threads[t], NULL, VectorMult, (void *) 
       &matrix_data_array1[t]);
  if (rc) {
    printf("ERROR; return code from pthread_create() is %d\n", rc);
    exit(-1);
    }
  }}


pthread_exit(NULL);
}
