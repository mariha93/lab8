
#include "matrix.h"
#include <time.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>


struct matrix_data matrix_data_array[NUM_THREADS];

void *MatrixMult(void *threadarg)
{
clock_t t;
   int taskid, rows, columns;
   int *marray1;
   int *marray2;
int *result1;
   int i = 0;
   int j=0;
   struct matrix_data *my_data;

   my_data = (struct matrix_data *) threadarg;
   taskid = my_data->thread_id;
   rows = my_data->rows;
   columns = my_data->columns;
   marray1 = my_data->marray1;
   marray2 = my_data->marray2;
   result1 = my_data->result1;

int k =0;

t = clock();

		for (i = taskid; i < 10; i=i+2) {
			for (j = 0; j < 10; j++) {
				for (k = 0; k < 10; k++) {
					result1[(i*10) + j] += marray1[(i*10) + k] * marray2[(k*10) + j];
				}
			}
		}
t = clock() - t;
double time_taken = ((double)t)/CLOCKS_PER_SEC;

/*for (i = 0; i < (10*10); i++) {
			printf("%d ",result1[i]);
			if (i % 10 == 1499) {
				printf("\n");
			}
		}*/
printf("Time taken: %f", time_taken);
pthread_exit(NULL);
}

