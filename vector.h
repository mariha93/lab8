#ifndef FUCTIONS1_H_INCLUDED
#define FUCTIONS1_H_INCLUDED

void *VectorMult(void *);

#endif

#ifndef NUM_THREADS
#define NUM_THREADS	2

#endif

#ifndef STRUCT1_H_INCLUDED
#define STRUCT1_H_INCLUDED

struct vector_data
{
   int	thread_id;
   int  rows;
   int columns;
   int *marray1;
   int *marray2;
   int *result1;
};

extern struct vector_data matrix_data_array1[NUM_THREADS];

#endif

